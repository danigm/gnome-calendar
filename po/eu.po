# Basque translation for gnome-calendar
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Iñaki Larrañaga Murgoitio <dooteo@zundan.com>, 2015, 2016, 2017.
# Asier Sarasua Garmendia <asiersarasua@ni.eus>, 2019, 2020, 2021, 2022, 2023.
# Porrumentzio <porrumentzio@riseup.net>, 2022.
#
msgid ""
msgstr "Project-Id-Version: gnome-calendar master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-calendar/issues\n"
"POT-Creation-Date: 2023-08-04 23:11+0000\n"
"PO-Revision-Date: 2023-08-13 19:41+0200\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <Librezale>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:7
#: data/org.gnome.Calendar.desktop.in.in:3 src/main.c:35
#: src/gui/gcal-application.c:178 src/gui/gcal-quick-add-popover.ui:135
#: src/gui/gcal-window.ui:4 src/gui/gcal-window.ui:118
msgid "Calendar"
msgstr "Egutegia"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:8
msgid "Calendar for GNOME"
msgstr "GNOMEren Egutegia"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:11
msgid ""
"GNOME Calendar is a simple and beautiful calendar application designed to "
"perfectly fit the GNOME desktop. By reusing the components which the GNOME "
"desktop is built on, Calendar nicely integrates with the GNOME ecosystem."
msgstr "GNOME Calendar egutegien aplikazio sotil eta ederra da, GNOME mahaigainean primeran integratu dadin diseinatua. GNOME mahaigainak oinarritzat dituen osagaiak berrerabilita, Calendar ederto integratzen da GNOME ekosisteman."

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:16
msgid ""
"We aim to find the perfect balance between nicely crafted features and user-"
"centred usability. No excess, nothing missing. You’ll feel comfortable using "
"Calendar, like you’ve been using it for ages!"
msgstr "Oreka egokia bilatu nahi dugu ongi eraikitako eginbideen eta erabiltzailean zentratutako erabilgarritasunaren artean. Ezer ez soberan, ezer ez faltan. Eroso arituko zara Calendar erabilita, betidanik erabili izan bazenu bezala!"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:24
msgid "Week view"
msgstr "Asteko ikuspegia"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:28
msgid "Month view"
msgstr "Hileko ikuspegia"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:32
msgid "Event editor"
msgstr "Gertaeren editorea"

#: data/org.gnome.Calendar.desktop.in.in:4
msgid "Access and manage your calendars"
msgstr "Atzitu eta kudeatu zure egutegiak"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calendar.desktop.in.in:13
msgid "Calendar;Event;Reminder;"
msgstr "Egutegia;Gertaera;Oroigarria;"

#: data/org.gnome.calendar.gschema.xml.in:6
msgid "Window maximized"
msgstr "Leihoa maximizatuta"

#: data/org.gnome.calendar.gschema.xml.in:7
msgid "Window maximized state"
msgstr "Leiho maximizatuaren egoera"

#: data/org.gnome.calendar.gschema.xml.in:11
msgid "Window size"
msgstr "Leihoaren tamaina"

#: data/org.gnome.calendar.gschema.xml.in:12
msgid "Window size (width and height)."
msgstr "Leihoaren tamaina (zabalera eta altuera)."

#: data/org.gnome.calendar.gschema.xml.in:16
msgid "Type of the active view"
msgstr "Ikuspegi aktiboaren mota"

#: data/org.gnome.calendar.gschema.xml.in:17
msgid "Type of the active window view, default value is: monthly view"
msgstr "Leihoaren ikuspegi aktiboaren mota. Balio lehenetsia: hileko ikuspegia"

#: data/org.gnome.calendar.gschema.xml.in:21
msgid "Weather Service Configuration"
msgstr "Eguraldi-zerbitzuaren konfigurazioa"

#: data/org.gnome.calendar.gschema.xml.in:22
msgid ""
"Whether weather reports are shown, automatic locations are used and a "
"location-name"
msgstr "Eguraldi-txostenak erakusten badira, kokaleku automatikoak eta kokalekuaren izen bat erabiliko dira"

#: data/org.gnome.calendar.gschema.xml.in:26
msgid "Zoom level of the week grid"
msgstr "Asteko saretaren zoom-maila"

#: data/org.gnome.calendar.gschema.xml.in:27
msgid "The current zoom level of the week grid"
msgstr "Asteko saretaren uneko zoom-maila"

#. Translators: %1$s is the start date and %2$s is the end date.
#. Translators: %1$s is the start date, and %2$s. For example: June 21 - November 29, 2022
#: src/core/gcal-event.c:1906 src/gui/gcal-event-popover.c:395
#, c-format
msgid "%1$s — %2$s"
msgstr "%1$s — %2$s"

#.
#. * Translators: %1$s is the start date, %2$s is the start time,
#. * %3$s is the end date, and %4$s is the end time.
#.
#: src/core/gcal-event.c:1914
#, c-format
msgid "%1$s %2$s — %3$s %4$s"
msgstr "%1$s %2$s — %3$s %4$s"

#. Translators: %1$s is a date, %2$s is the start hour, and %3$s is the end hour
#. Translators: %1$s is the event name, %2$s is the start hour, and %3$s is the end hour
#: src/core/gcal-event.c:1930 src/gui/gcal-quick-add-popover.c:461
#, c-format
msgid "%1$s, %2$s – %3$s"
msgstr "%1$s, %2$s – %3$s"

#: src/gui/calendar-management/gcal-calendar-management-dialog.ui:4
msgid "Calendar Settings"
msgstr "Egutegiaren ezarpenak"

#: src/gui/calendar-management/gcal-calendars-page.c:343
#: src/gui/calendar-management/gcal-new-calendar-page.ui:141
msgid "Calendars"
msgstr "Egutegiak"

#. Create the new toast
#: src/gui/calendar-management/gcal-calendars-page.c:368
#, c-format
msgid "Calendar <b>%s</b> removed"
msgstr "<b>%s</b> egutegia kenduta"

#: src/gui/calendar-management/gcal-calendars-page.c:373
#: src/gui/gcal-window.c:741
msgid "_Undo"
msgstr "_Desegin"

#: src/gui/calendar-management/gcal-calendars-page.ui:55
msgid "Add Calendar…"
msgstr "Gehitu egutegia…"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:22
msgid "Account"
msgstr "Kontua"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:30
msgid "Open in Settings"
msgstr "Ireki hobespenetan"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:53
#: src/gui/event-editor/gcal-summary-section.ui:23
#: src/gui/gcal-event-popover.ui:113
#: src/gui/importer/gcal-import-file-row.c:151
msgid "Location"
msgstr "Kokalekua"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:65
#: src/gui/calendar-management/gcal-new-calendar-page.ui:24
msgid "Calendar Name"
msgstr "Egutegiaren izena"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:72
#: src/gui/calendar-management/gcal-new-calendar-page.ui:32
msgid "Color"
msgstr "Kolorea"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:93
msgid "_Display Calendar"
msgstr "_Bistaratu egutegia"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:100
msgid "Default _Calendar"
msgstr "_Egutegi lehenetsia"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:101
msgid "Add new events to this calendar by default."
msgstr "Gehitu gertaera berriak egutegi honi, lehenespenez."

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:109
msgid "_Remove Calendar"
msgstr "_Kendu egutegia"

#: src/gui/calendar-management/gcal-file-chooser-button.c:51
msgid "Select a file"
msgstr "Hautatu fitxategia"

#: src/gui/calendar-management/gcal-new-calendar-page.c:158
#: src/gui/calendar-management/gcal-new-calendar-page.c:165
#: src/gui/calendar-management/gcal-new-calendar-page.ui:190
#, c-format
msgid "Add Calendar"
msgid_plural "Add %1$u Calendars"
msgstr[0] "Gehitu egutegia"
msgstr[1] "Gehitu %1$u egutegi"

#: src/gui/calendar-management/gcal-new-calendar-page.c:516
msgid "New Calendar"
msgstr "Egutegi berria"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:60
msgid "Import a Calendar"
msgstr "Inportatu egutegi bat"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:77
msgid ""
"Alternatively, enter the web address of an online calendar you want to "
"import, or open a supported calendar file."
msgstr "Bestela, sartu inportatu nahi duzun lineako egutegi baten web helbidea, edo ireki onartuta dagoen egutegi-fitxategi bat."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:105
msgid "Open a File"
msgstr "Ireki fitxategia"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:108
msgid "Calendar files"
msgstr "Egutegiaren fitxategiak"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:165
msgid ""
"If the calendar belongs to one of your online accounts, you can add it "
"through the <a href=\"GOA\">online account settings</a>."
msgstr "Egutegia zure lineako kontu batena bada, <a href=\"GOA\">lineako kontuaren ezarpenak</a> bidez gehi dezakezu."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:213
msgid "Credentials"
msgstr "Kredentzialak"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:221
msgid "User"
msgstr "Erabiltzailea"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:226
msgid "Password"
msgstr "Pasahitza"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:237
#: src/gui/event-editor/gcal-event-editor-dialog.ui:28
#: src/gui/importer/gcal-import-dialog.ui:20 src/utils/gcal-utils.c:1301
msgid "_Cancel"
msgstr "_Utzi"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:238
msgid "_Connect"
msgstr "_Konektatu"

#: src/gui/event-editor/gcal-alarm-row.c:84
#, c-format
msgid "%1$u day, %2$u hour, and %3$u minute before"
msgid_plural "%1$u day, %2$u hour, and %3$u minutes before"
msgstr[0] "Egun %1$u, ordu %2$u eta minutu %3$u lehenago"
msgstr[1] "Egun %1$u, ordu %2$u eta %3$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:88
#, c-format
msgid "%1$u day, %2$u hours, and %3$u minute before"
msgid_plural "%1$u day, %2$u hours, and %3$u minutes before"
msgstr[0] "Egun %1$u, %2$u ordu eta minutu %3$u lehenago"
msgstr[1] "Egun %1$u, %2$u ordu eta %3$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:94
#, c-format
msgid "%1$u days, %2$u hour, and %3$u minute before"
msgid_plural "%1$u days, %2$u hour, and %3$u minutes before"
msgstr[0] "%1$u egun, ordu %2$u eta minutu %3$u lehenago"
msgstr[1] "%1$u egun, ordu %2$u eta %3$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:98
#, c-format
msgid "%1$u days, %2$u hours, and %3$u minute before"
msgid_plural "%1$u days, %2$u hours, and %3$u minutes before"
msgstr[0] "%1$u egun, %2$u ordu eta minutu %3$u lehenago"
msgstr[1] "%1$u egun, %2$u ordu eta %3$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:113
#, c-format
msgid "%1$u day and %2$u hour before"
msgid_plural "%1$u day and %2$u hours before"
msgstr[0] "Egun %1$u eta ordu %2$u lehenago"
msgstr[1] "Egun %1$u eta %2$u ordu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:117
#, c-format
msgid "%1$u days and %2$u hour before"
msgid_plural "%1$u days and %2$u hours before"
msgstr[0] "%1$u egun eta ordu %2$u lehenago"
msgstr[1] "%1$u egun eta %2$u ordu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:133
#, c-format
msgid "%1$u day and %2$u minute before"
msgid_plural "%1$u day and %2$u minutes before"
msgstr[0] "Egun %1$u eta minutu %2$u lehenago"
msgstr[1] "Egun %1$u eta %2$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:137
#, c-format
msgid "%1$u days and %2$u minute before"
msgid_plural "%1$u days and %2$u minutes before"
msgstr[0] "%1$u egun eta minutu %2$u lehenago"
msgstr[1] "%1$u egun eta %2$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:147
#, c-format
msgid "%1$u day before"
msgid_plural "%1$u days before"
msgstr[0] "Egun %1$u lehenago"
msgstr[1] "%1$u egun lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:165
#, c-format
msgid "%1$u hour and %2$u minute before"
msgid_plural "%1$u hour and %2$u minutes before"
msgstr[0] "Ordu %1$u eta minutu %2$u lehenago"
msgstr[1] "Ordu %1$u eta %2$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:169
#, c-format
msgid "%1$u hours and %2$u minute before"
msgid_plural "%1$u hours and %2$u minutes before"
msgstr[0] "%1$u ordu eta minutu %2$u lehenago"
msgstr[1] "%1$u ordu eta %2$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:179
#, c-format
msgid "%1$u hour before"
msgid_plural "%1$u hours before"
msgstr[0] "Ordu %1$u lehenago"
msgstr[1] "%1$u ordu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:191
#, c-format
msgid "%1$u minute before"
msgid_plural "%1$u minutes before"
msgstr[0] "Minutu %1$u lehenago"
msgstr[1] "%1$u minutu lehenago"

#: src/gui/event-editor/gcal-alarm-row.c:198
msgid "Event start time"
msgstr "Gertaeraren hasierako ordua"

#: src/gui/event-editor/gcal-alarm-row.ui:11
msgid "Toggles the sound of the alarm"
msgstr "Alarmaren soinua aktibatzen/desaktibatzen du"

#: src/gui/event-editor/gcal-alarm-row.ui:21
msgid "Remove the alarm"
msgstr "Kendu alarma"

#: src/gui/event-editor/gcal-event-editor-dialog.c:187
msgid "Save"
msgstr "Gorde"

#: src/gui/event-editor/gcal-event-editor-dialog.c:187
msgid "Done"
msgstr "Eginda"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:42
msgid "Click to select the calendar"
msgstr "Egin klik egutegia hautatzeko"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:100
msgid "_Done"
msgstr "_Eginda"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:127
msgid "Schedule"
msgstr "Antolaketa"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:139
msgid "Reminders"
msgstr "Oroigarriak"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:151
msgid "Notes"
msgstr "Oharrak"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:171
msgid "Delete Event"
msgstr "Ezabatu gertaera"

#: src/gui/event-editor/gcal-reminders-section.ui:26
msgid "Add a Reminder…"
msgstr "Gehitu oroigarria…"

#: src/gui/event-editor/gcal-reminders-section.ui:52
msgid "5 minutes"
msgstr "5 minutu"

#: src/gui/event-editor/gcal-reminders-section.ui:58
msgid "10 minutes"
msgstr "10 minutu"

#: src/gui/event-editor/gcal-reminders-section.ui:64
msgid "15 minutes"
msgstr "15 minutu"

#: src/gui/event-editor/gcal-reminders-section.ui:70
msgid "30 minutes"
msgstr "30 minutu"

#: src/gui/event-editor/gcal-reminders-section.ui:76
msgid "1 hour"
msgstr "Ordu 1"

#: src/gui/event-editor/gcal-reminders-section.ui:82
msgid "1 day"
msgstr "Egun 1"

#: src/gui/event-editor/gcal-reminders-section.ui:88
msgid "2 days"
msgstr "2 egun"

#: src/gui/event-editor/gcal-reminders-section.ui:94
msgid "3 days"
msgstr "3 egun"

#: src/gui/event-editor/gcal-reminders-section.ui:100
msgid "1 week"
msgstr "Aste 1"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:238
#, c-format
msgid "Last %A"
msgstr "Azken %A"

#: src/gui/event-editor/gcal-schedule-section.c:242
#: src/gui/gcal-event-popover.c:207 src/gui/gcal-event-popover.c:321
#: src/gui/views/gcal-agenda-view.c:186
msgid "Yesterday"
msgstr "Atzo"

#: src/gui/event-editor/gcal-schedule-section.c:246
#: src/gui/gcal-event-popover.c:199 src/gui/gcal-event-popover.c:313
#: src/gui/views/gcal-agenda-view.c:182
msgid "Today"
msgstr "Gaur"

#: src/gui/event-editor/gcal-schedule-section.c:250
#: src/gui/gcal-event-popover.c:203 src/gui/gcal-event-popover.c:317
#: src/gui/views/gcal-agenda-view.c:184
msgid "Tomorrow"
msgstr "Bihar"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:255
#, c-format
msgid "This %A"
msgstr "Datorren %A"

#.
#. * Translators: %1$s is the formatted date (e.g. Today, Sunday, or even 2019-10-11) and %2$s is the
#. * formatted time (e.g. 03:14 PM, or 21:29)
#.
#: src/gui/event-editor/gcal-schedule-section.c:285
#, c-format
msgid "%1$s, %2$s"
msgstr "%1$s, %2$s"

#: src/gui/event-editor/gcal-schedule-section.ui:17
msgid "All Day"
msgstr "Egun osoa"

#: src/gui/event-editor/gcal-schedule-section.ui:34
#: src/gui/importer/gcal-import-file-row.c:152
msgid "Starts"
msgstr "Hasten da"

#: src/gui/event-editor/gcal-schedule-section.ui:77
#: src/gui/importer/gcal-import-file-row.c:153
msgid "Ends"
msgstr "Amaitzen da"

#: src/gui/event-editor/gcal-schedule-section.ui:120
msgid "Repeat"
msgstr "Errepikatu"

#: src/gui/event-editor/gcal-schedule-section.ui:125
msgid "No Repeat"
msgstr "Ez errepikatu"

#: src/gui/event-editor/gcal-schedule-section.ui:126
msgid "Daily"
msgstr "Egunero"

#: src/gui/event-editor/gcal-schedule-section.ui:127
msgid "Monday – Friday"
msgstr "Astelehena – Ostirala"

#: src/gui/event-editor/gcal-schedule-section.ui:128
msgid "Weekly"
msgstr "Astero"

#: src/gui/event-editor/gcal-schedule-section.ui:129
msgid "Monthly"
msgstr "Hilero"

#: src/gui/event-editor/gcal-schedule-section.ui:130
msgid "Yearly"
msgstr "Urtero"

#: src/gui/event-editor/gcal-schedule-section.ui:141
msgid "End Repeat"
msgstr "Amaitu errepikapena"

#: src/gui/event-editor/gcal-schedule-section.ui:146
msgid "Forever"
msgstr "Beti"

#: src/gui/event-editor/gcal-schedule-section.ui:147
msgid "No. of occurrences"
msgstr "Agerraldien kopurua"

#: src/gui/event-editor/gcal-schedule-section.ui:148
msgid "Until Date"
msgstr "Data arte"

#: src/gui/event-editor/gcal-schedule-section.ui:160
msgid "Number of Occurrences"
msgstr "Agerraldien kopurua"

#: src/gui/event-editor/gcal-schedule-section.ui:179
msgid "End Repeat Date"
msgstr "Errepikapen-amaieraren data"

#: src/gui/event-editor/gcal-summary-section.c:78
#: src/gui/gcal-quick-add-popover.c:676
msgid "Unnamed event"
msgstr "Gertaera izengabea"

#: src/gui/event-editor/gcal-summary-section.ui:16
#: src/gui/importer/gcal-import-file-row.c:150
msgid "Title"
msgstr "Titulua"

#: src/gui/event-editor/gcal-time-selector.ui:22
msgid ":"
msgstr ":"

#: src/gui/event-editor/gcal-time-selector.ui:46
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "AM"
msgstr "AM"

#: src/gui/event-editor/gcal-time-selector.ui:47
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "PM"
msgstr "PM"

#: src/gui/gcal-application.c:58
msgid "Quit GNOME Calendar"
msgstr "Irten GNOMEren Egutegia aplikaziotik"

#: src/gui/gcal-application.c:63
msgid "Display version number"
msgstr "Erakutsi bertsio-zenbakia"

#: src/gui/gcal-application.c:68
msgid "Enable debug messages"
msgstr "Gaitu mezuak araztea"

#: src/gui/gcal-application.c:73
msgid "Open calendar on the passed date"
msgstr "Ireki egutegia igarotako datan"

#: src/gui/gcal-application.c:78
msgid "Open calendar showing the passed event"
msgstr "Ireki egutegia igarotako gertaera erakutsiz"

#: src/gui/gcal-application.c:144
#, c-format
msgid "Copyright © 2012–%d The Calendar authors"
msgstr "Copyright © 2012–%d Egutegiaren egileak"

#: src/gui/gcal-application.c:180
msgid "The GNOME Project"
msgstr "GNOME proiektua"

#: src/gui/gcal-application.c:189
msgid "translator-credits"
msgstr "translator-credits"

#: src/gui/gcal-application.c:194 src/gui/gcal-window.ui:326
msgid "Weather"
msgstr "Eguraldia"

#: src/gui/gcal-calendar-button.ui:6
msgid "Manage Calendars"
msgstr "Kudeatu egutegiak"

#: src/gui/gcal-calendar-button.ui:8
msgid "_Calendars"
msgstr "_Egutegiak"

#: src/gui/gcal-calendar-button.ui:37
msgid "_Synchronize Calendars"
msgstr "_Sinkronizatu egutegiak"

#: src/gui/gcal-calendar-button.ui:41
msgid "Manage Calendars…"
msgstr "Kudeatu egutegiak…"

#: src/gui/gcal-event-popover.c:122 src/gui/gcal-quick-add-popover.c:255
msgid "January"
msgstr "Urtarrila"

#: src/gui/gcal-event-popover.c:123 src/gui/gcal-quick-add-popover.c:256
msgid "February"
msgstr "Otsaila"

#: src/gui/gcal-event-popover.c:124 src/gui/gcal-quick-add-popover.c:257
msgid "March"
msgstr "Martxoa"

#: src/gui/gcal-event-popover.c:125 src/gui/gcal-quick-add-popover.c:258
msgid "April"
msgstr "Apirila"

#: src/gui/gcal-event-popover.c:126 src/gui/gcal-quick-add-popover.c:259
msgid "May"
msgstr "Maiatza"

#: src/gui/gcal-event-popover.c:127 src/gui/gcal-quick-add-popover.c:260
msgid "June"
msgstr "Ekaina"

#: src/gui/gcal-event-popover.c:128 src/gui/gcal-quick-add-popover.c:261
msgid "July"
msgstr "Uztaila"

#: src/gui/gcal-event-popover.c:129 src/gui/gcal-quick-add-popover.c:262
msgid "August"
msgstr "Abuztua"

#: src/gui/gcal-event-popover.c:130 src/gui/gcal-quick-add-popover.c:263
msgid "September"
msgstr "Iraila"

#: src/gui/gcal-event-popover.c:131 src/gui/gcal-quick-add-popover.c:264
msgid "October"
msgstr "Urria"

#: src/gui/gcal-event-popover.c:132 src/gui/gcal-quick-add-popover.c:265
msgid "November"
msgstr "Azaroa"

#: src/gui/gcal-event-popover.c:133 src/gui/gcal-quick-add-popover.c:266
msgid "December"
msgstr "Abendua"

#: src/gui/gcal-event-popover.c:158
#, c-format
msgid "Today %s"
msgstr "Gaur %s"

#: src/gui/gcal-event-popover.c:162
#, c-format
msgid "Tomorrow %s"
msgstr "Bihar %s"

#: src/gui/gcal-event-popover.c:166
#, c-format
msgid "Yesterday %s"
msgstr "Atzo %s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$ is the hour. This format string results in dates
#. * like "November 21, 22:00".
#.
#: src/gui/gcal-event-popover.c:175
#, c-format
msgid "%1$s %2$d, %3$s"
msgstr "%1$s %2$d, %3$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, and %4$s is the hour. This format string
#. * results in dates like "November 21, 2020, 22:00".
#.
#: src/gui/gcal-event-popover.c:187
#, c-format
msgid "%1$s %2$d, %3$d, %4$s"
msgstr "%1$s %2$d, %3$d, %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), and %2$d is
#. * the day of month. This format string results in dates like
#. * "November 21".
#.
#: src/gui/gcal-event-popover.c:216 src/gui/gcal-event-popover.c:330
#, c-format
msgid "%1$s %2$d"
msgstr "%1$s %2$d"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$d is the year. This format string results in dates
#. * like "November 21, 2020".
#.
#: src/gui/gcal-event-popover.c:227 src/gui/gcal-event-popover.c:341
#, c-format
msgid "%1$s %2$d, %3$d"
msgstr "%3$d %1$s %2$d"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Today, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:261
#, c-format
msgid "Today, %1$s — %2$s"
msgstr "Gaur, %1$s — %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:269
#, c-format
msgid "Tomorrow, %1$s – %2$s"
msgstr "Bihar, %1$s – %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:277
#, c-format
msgid "Yesterday, %1$s – %2$s"
msgstr "Atzo, %1$s – %2$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$s is the start hour, and %4$s is the end hour. This
#. * format string results in dates like "November 21, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:286
#, c-format
msgid "%1$s %2$d, %3$s – %4$s"
msgstr "%1$s %2$d, %3$s – %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, %4$s is the start hour, and %5$s is the
#. * end hour. This format string results in dates like:
#. *
#. * "November 21, 2021, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:301
#, c-format
msgid "%1$s %2$d, %3$d, %4$s – %5$s"
msgstr "%3$d %1$s %2$d, %4$s – %5$s"

#: src/gui/gcal-event-popover.ui:73
msgid "No event information"
msgstr "Ez dago gertaeraren informaziorik"

#: src/gui/gcal-event-popover.ui:171
msgid "Edit…"
msgstr "Editatu…"

#. Translators: %s is the location of the event (e.g. "Downtown, 3rd Avenue")
#: src/gui/gcal-event-widget.c:351
#, c-format
msgid "At %s"
msgstr "Lekua: %s"

#: src/gui/gcal-meeting-row.c:67
msgid "Google Meet"
msgstr "Google Meet"

#: src/gui/gcal-meeting-row.c:68
msgid "Jitsi"
msgstr "Jitsi"

#: src/gui/gcal-meeting-row.c:69
msgid "Whereby"
msgstr "Whereby"

#: src/gui/gcal-meeting-row.c:70
msgid "Zoom"
msgstr "Zoom"

#: src/gui/gcal-meeting-row.c:71
msgid "Microsoft Teams"
msgstr "Microsoft Teams"

#: src/gui/gcal-meeting-row.c:81
msgid "Unknown Service"
msgstr "Zerbitzu ezezaguna"

#. Translators: "Join" as in "Join meeting"
#: src/gui/gcal-meeting-row.ui:13
msgid "Join"
msgstr "Elkartu"

#: src/gui/gcal-quick-add-popover.c:118
#, c-format
msgid "%s (this calendar is read-only)"
msgstr "%s (egutegi hau irakurtzeko soilik da)"

#: src/gui/gcal-quick-add-popover.c:233
msgid "from next Monday"
msgstr "datorren astelehenetik"

#: src/gui/gcal-quick-add-popover.c:234
msgid "from next Tuesday"
msgstr "datorren asteartetik"

#: src/gui/gcal-quick-add-popover.c:235
msgid "from next Wednesday"
msgstr "datorren asteazkenetik"

#: src/gui/gcal-quick-add-popover.c:236
msgid "from next Thursday"
msgstr "datorren ostegunetik"

#: src/gui/gcal-quick-add-popover.c:237
msgid "from next Friday"
msgstr "datorren ostiraletik"

#: src/gui/gcal-quick-add-popover.c:238
msgid "from next Saturday"
msgstr "datorren larunbatetik"

#: src/gui/gcal-quick-add-popover.c:239
msgid "from next Sunday"
msgstr "datorren igandetik"

#: src/gui/gcal-quick-add-popover.c:244
msgid "to next Monday"
msgstr "datorren astelehenera arte"

#: src/gui/gcal-quick-add-popover.c:245
msgid "to next Tuesday"
msgstr "datorren asteartera arte"

#: src/gui/gcal-quick-add-popover.c:246
msgid "to next Wednesday"
msgstr "datorren asteazkenera arte"

#: src/gui/gcal-quick-add-popover.c:247
msgid "to next Thursday"
msgstr "datorren ostegunera arte"

#: src/gui/gcal-quick-add-popover.c:248
msgid "to next Friday"
msgstr "datorren ostiralera arte"

#: src/gui/gcal-quick-add-popover.c:249
msgid "to next Saturday"
msgstr "datorren larunbatera arte"

#: src/gui/gcal-quick-add-popover.c:250
msgid "to next Sunday"
msgstr "datorren igandera arte"

#: src/gui/gcal-quick-add-popover.c:275
#, c-format
msgid "from Today"
msgstr "gaurtik"

#: src/gui/gcal-quick-add-popover.c:279
#, c-format
msgid "from Tomorrow"
msgstr "bihartik"

#: src/gui/gcal-quick-add-popover.c:283
#, c-format
msgid "from Yesterday"
msgstr "atzotik"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:301
#, c-format
msgid "from %1$s %2$s"
msgstr "Noiztik: %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:312
#, c-format
msgid "to Today"
msgstr "gaur arte"

#: src/gui/gcal-quick-add-popover.c:316
#, c-format
msgid "to Tomorrow"
msgstr "atzo arte"

#: src/gui/gcal-quick-add-popover.c:320
#, c-format
msgid "to Yesterday"
msgstr "bihar arte"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:338
#, c-format
msgid "to %1$s %2$s"
msgstr "Nora: %1$s %2$s"

#. Translators: %1$s is the start date (e.g. "from Today") and %2$s is the end date (e.g. "to Tomorrow")
#: src/gui/gcal-quick-add-popover.c:345
#, c-format
msgid "New Event %1$s %2$s"
msgstr "Gertaera berria: %1$s %2$s"

#: src/gui/gcal-quick-add-popover.c:362
#, c-format
msgid "New Event Today"
msgstr "Gertaera berria: gaur"

#: src/gui/gcal-quick-add-popover.c:366
#, c-format
msgid "New Event Tomorrow"
msgstr "Gertaera berria: bihar"

#: src/gui/gcal-quick-add-popover.c:370
#, c-format
msgid "New Event Yesterday"
msgstr "Gertaera berria: atzo"

#: src/gui/gcal-quick-add-popover.c:376
msgid "New Event next Monday"
msgstr "Gertaera berria: datorren astelehenean"

#: src/gui/gcal-quick-add-popover.c:377
msgid "New Event next Tuesday"
msgstr "Gertaera berria: datorren asteartean"

#: src/gui/gcal-quick-add-popover.c:378
msgid "New Event next Wednesday"
msgstr "Gertaera berria: datorren asteazkenean"

#: src/gui/gcal-quick-add-popover.c:379
msgid "New Event next Thursday"
msgstr "Gertaera berria: datorren ostegunean"

#: src/gui/gcal-quick-add-popover.c:380
msgid "New Event next Friday"
msgstr "Gertaera berria: datorren ostiralean"

#: src/gui/gcal-quick-add-popover.c:381
msgid "New Event next Saturday"
msgstr "Gertaera berria: datorren larunbatean"

#: src/gui/gcal-quick-add-popover.c:382
msgid "New Event next Sunday"
msgstr "Gertaera berria: datorren igandean"

#. Translators: %d is the numeric day of month
#: src/gui/gcal-quick-add-popover.c:394
#, c-format
msgid "New Event on January %d"
msgstr "Gertaera berria: urtarrilak %d"

#: src/gui/gcal-quick-add-popover.c:395
#, c-format
msgid "New Event on February %d"
msgstr "Gertaera berria: otsailak %d"

#: src/gui/gcal-quick-add-popover.c:396
#, c-format
msgid "New Event on March %d"
msgstr "Gertaera berria: martxoak  %d"

#: src/gui/gcal-quick-add-popover.c:397
#, c-format
msgid "New Event on April %d"
msgstr "Gertaera berria: apirilak %d"

#: src/gui/gcal-quick-add-popover.c:398
#, c-format
msgid "New Event on May %d"
msgstr "Gertaera berria: maiatzak %d"

#: src/gui/gcal-quick-add-popover.c:399
#, c-format
msgid "New Event on June %d"
msgstr "Gertaera berria: ekainak %d"

#: src/gui/gcal-quick-add-popover.c:400
#, c-format
msgid "New Event on July %d"
msgstr "Gertaera berria: uztailak %d"

#: src/gui/gcal-quick-add-popover.c:401
#, c-format
msgid "New Event on August %d"
msgstr "Gertaera berria: abuztuak %d"

#: src/gui/gcal-quick-add-popover.c:402
#, c-format
msgid "New Event on September %d"
msgstr "Gertaera berria: irailak %d"

#: src/gui/gcal-quick-add-popover.c:403
#, c-format
msgid "New Event on October %d"
msgstr "Gertaera berria: urriak %d"

#: src/gui/gcal-quick-add-popover.c:404
#, c-format
msgid "New Event on November %d"
msgstr "Gertaera berria: azaroak %d"

#: src/gui/gcal-quick-add-popover.c:405
#, c-format
msgid "New Event on December %d"
msgstr "Gertaera berria: abenduak %d"

#: src/gui/gcal-quick-add-popover.ui:87
msgid "Edit Details…"
msgstr "Editatu xehetasunak…"

#: src/gui/gcal-quick-add-popover.ui:98
msgid "Add"
msgstr "Gehitu"

#: src/gui/gcal-search-button.ui:53
msgid "Search events"
msgstr "Bilatu gertaerak"

#: src/gui/gcal-sync-indicator.ui:21
msgctxt "tooltip"
msgid "Synchronizing Remote Calendars…"
msgstr "Urreneko egutegiak sinkronizatzen…"

#: src/gui/gcal-toolbar-end.ui:9
msgctxt "tooltip"
msgid "Search"
msgstr "Bilatu"

#: src/gui/gcal-toolbar-end.ui:16
msgctxt "tooltip"
msgid "Add Event"
msgstr "Gehitu gertaea"

#: src/gui/gcal-weather-settings.ui:17
msgid "Show Weather"
msgstr "Erakutsi eguraldia"

#: src/gui/gcal-weather-settings.ui:34
msgid "Automatic Location"
msgstr "Kokaleku automatikoa"

#: src/gui/gcal-weather-settings.ui:46
msgid "Search locations"
msgstr "Bilatu kokalekuak"

#: src/gui/gcal-window.c:337
msgid "Week"
msgstr "Astea"

#: src/gui/gcal-window.c:339
msgid "Month"
msgstr "Hila"

#: src/gui/gcal-window.c:739
msgid "Another event deleted"
msgstr "Beste gertaera ezabatuta"

#: src/gui/gcal-window.c:739
msgid "Event deleted"
msgstr "Gertaera ezabatuta"

#: src/gui/gcal-window.ui:137
msgid "Main Menu"
msgstr "Menu nagusia"

#: src/gui/gcal-window.ui:174 src/gui/gcal-window.ui:213
msgid "_Today"
msgstr "_Gaur"

#: src/gui/gcal-window.ui:249
msgid "_Week"
msgstr "_Astea"

#: src/gui/gcal-window.ui:264
msgid "_Month"
msgstr "_Hilabetea"

#: src/gui/gcal-window.ui:318
msgid "_Date & Time Settings…"
msgstr "_Data eta orduaren ezarpenak…"

#: src/gui/gcal-window.ui:322
msgid "_Online Accounts…"
msgstr "_Lineako kontuak…"

#: src/gui/gcal-window.ui:334
msgid "_Keyboard Shortcuts"
msgstr "Las_ter-teklak"

#: src/gui/gcal-window.ui:338
msgid "_About Calendar"
msgstr "Egutegia aplikazioari _buruz"

#: src/gui/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Orokorra"

#: src/gui/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "New event"
msgstr "Gertaera berria"

#: src/gui/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Manage calendars"
msgstr "Kudeatu egutegiak"

#: src/gui/gtk/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Synchronize calendars"
msgstr "Sinkronizatu egutegiak"

#: src/gui/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Close window"
msgstr "Itxi leihoa"

#: src/gui/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Search"
msgstr "Bilatu"

#: src/gui/gtk/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Show help"
msgstr "Erakutsi laguntza"

#: src/gui/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "Lasterbideak"

#: src/gui/gtk/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Nabigazioa"

#: src/gui/gtk/help-overlay.ui:61
msgctxt "shortcut window"
msgid "Go back"
msgstr "Joan atzera"

#: src/gui/gtk/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Go forward"
msgstr "Joan aurrera"

#: src/gui/gtk/help-overlay.ui:73
msgctxt "shortcut window"
msgid "Show today"
msgstr "Erakutsi gaur"

#: src/gui/gtk/help-overlay.ui:79
msgctxt "shortcut window"
msgid "Next view"
msgstr "Ikuspegi berria"

#: src/gui/gtk/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Previous view"
msgstr "Aurreko ikuspegia"

#: src/gui/gtk/help-overlay.ui:93
msgctxt "shortcut window"
msgid "View"
msgstr "Ikusi"

#: src/gui/gtk/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Week view"
msgstr "Asteko ikuspegia"

#: src/gui/gtk/help-overlay.ui:102
msgctxt "shortcut window"
msgid "Month view"
msgstr "Hileko ikuspegia"

#: src/gui/importer/gcal-import-dialog.c:396
#, c-format
msgid "Import %d event"
msgid_plural "Import %d events"
msgstr[0] "Inportatu gertaera %d"
msgstr[1] "Inportatu %d gertaera"

#: src/gui/importer/gcal-import-dialog.ui:4
msgid "Import Files…"
msgstr "Inportau fitxategiak…"

#: src/gui/importer/gcal-import-dialog.ui:29
msgid "_Import"
msgstr "_Inportatu"

#: src/gui/importer/gcal-import-dialog.ui:49
msgid "C_alendar"
msgstr "_Egutegia"

#: src/gui/importer/gcal-importer.c:33
msgid "No error"
msgstr "Errorerik ez"

#: src/gui/importer/gcal-importer.c:36
msgid "Bad argument to function"
msgstr "Funtziorako argumentu okerra"

#: src/gui/importer/gcal-importer.c:40
msgid "Failed to allocate a new object in memory"
msgstr "Huts egin du objektu berri bat memorian esleitzeak"

#: src/gui/importer/gcal-importer.c:43
msgid "File is malformed, invalid, or corrupted"
msgstr "Fitxategia gaizki osatuta dago, baliogabea da edo hondatuta dago"

#: src/gui/importer/gcal-importer.c:46
msgid "Failed to parse the calendar contents"
msgstr "Huts egin du egutegi-edukiak analizatzeak"

#: src/gui/importer/gcal-importer.c:49
msgid "Failed to read file"
msgstr "Huts egin du fitxategiaren irakurketak"

#: src/gui/importer/gcal-importer.c:56
msgid "Internal error"
msgstr "Barneko errorea"

#: src/gui/importer/gcal-importer.c:94
msgid "File is not an iCalendar (.ics) file"
msgstr "Fitxategia ez da iCalendar (.ics) motakoa"

#: src/gui/importer/gcal-import-file-row.c:154
msgid "Description"
msgstr "Deskribapena"

#: src/gui/views/gcal-agenda-view.c:366
msgid "On-going"
msgstr "Abian"

#: src/gui/views/gcal-agenda-view.ui:22
msgid "No events"
msgstr "Gertaerarik ez"

#: src/gui/views/gcal-month-popover.ui:59
msgid "New Event…"
msgstr "Gertaera berria…"

#: src/gui/views/gcal-week-grid.c:576
msgid "00 AM"
msgstr "00 AM"

#: src/gui/views/gcal-week-grid.c:579
msgid "00:00"
msgstr "00:00"

#: src/gui/views/gcal-week-header.c:472
#, c-format
msgid "Other event"
msgid_plural "Other %d events"
msgstr[0] "Beste gertaera bat"
msgstr[1] "Beste %d gertaera"

#: src/gui/views/gcal-week-header.c:1001
#, c-format
msgid "week %d"
msgstr "%d. astea"

#: src/utils/gcal-utils.c:1297
msgid "Modify Multiple Events?"
msgstr "Aldatu gertaera anitz?"

#: src/utils/gcal-utils.c:1298
msgid ""
"The event you are trying to modify is recurring. The changes you have "
"selected should be applied to:"
msgstr "Aldatu nahi duzun gertaera errepikakorra da. Hautatu dituzun aldaketek honi eragingo die:"

#: src/utils/gcal-utils.c:1302
msgid "_Only This Event"
msgstr "_Soilik gertaera hau"

#: src/utils/gcal-utils.c:1308
msgid "_Subsequent Events"
msgstr "_Hurrengo gertaerak"

#: src/utils/gcal-utils.c:1311
msgid "_All Events"
msgstr "_Gertaera guztiak"

#~ msgid "Undo"
#~ msgstr "Desegin"

#~ msgid "Settings"
#~ msgstr "Ezarpenak"

#~ msgid "Calendar name"
#~ msgstr "Egutegiaren izena"

#~ msgid "Cancel"
#~ msgstr "Utzi"

#~ msgid "Manage your calendars"
#~ msgstr "Kudeatu zure egutegiak"

#~ msgctxt "tooltip"
#~ msgid "Search for events"
#~ msgstr "Bilatu gertaerak"

#~ msgctxt "tooltip"
#~ msgid "Add a new event"
#~ msgstr "Gehitu gertaera berria"

#~ msgid "Window position"
#~ msgstr "Leihoaren posizioa"

#~ msgid "Window position (x and y)."
#~ msgstr "Leihoaren posizioa (x eta y)."

#~ msgid "Open"
#~ msgstr "Ireki"

#~ msgid "Year view"
#~ msgstr "Urtearen ikuspegia"

#~ msgid "Year"
#~ msgstr "Urtea"

#~ msgctxt "shortcut window"
#~ msgid "Year view"
#~ msgstr "Urtearen ikuspegia"

#~ msgid "Follow system night light"
#~ msgstr "Jarraitu sistemaren gaueko argia"

#~ msgid "Use GNOME night light setting to activate night-mode."
#~ msgstr "Erabili GNOMEren gaueko argiaren ezarpena gaueko modua aktibatzeko."

#~ msgid "%B %d…"
#~ msgstr "%B %d…"

#~ msgid "%B %d"
#~ msgstr "%B %d"

#~ msgid "Check this out!"
#~ msgstr "Begiratu hau!"

#~ msgid "Calendar management"
#~ msgstr "Egutegiaren kudeaketa"

#~ msgid "Date"
#~ msgstr "Data"

#~ msgid "Time"
#~ msgstr "Ordua"

#~ msgid "From Web…"
#~ msgstr "Webetik…"

#~ msgid "New Local Calendar…"
#~ msgstr "Lokaleko egutegi berria…"

#~ msgid "No results found"
#~ msgstr "Ez da emaitzarik aurkitu"

#~ msgid "Try a different search"
#~ msgstr "Saiatu bestelako bilaketa"

#~ msgid "%d week before"
#~ msgid_plural "%d weeks before"
#~ msgstr[0] "Aste 1 lehenago"
#~ msgstr[1] "%d aste lehenago"

#~ msgid "%s AM"
#~ msgstr "%s AM"

#~ msgid "%s PM"
#~ msgstr "%s PM"
